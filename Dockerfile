FROM python:3.11-slim-bookworm

RUN apt-get update && apt-get install -y openjdk-17-jre-headless && apt-get clean autoclean && apt-get autoremove --yes && rm -rf /var/lib/{apt,dpkg,cache,log}/

RUN pip install pip setuptools wheel 
RUN pip install -U Red-DiscordBot

RUN useradd -mu 1014 red

USER 1014:1014

WORKDIR /home/red
VOLUME /home/red
